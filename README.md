Program that evaluates arithmetic expressions written in Polish notation. Expressions can contain 
double-precision floating point numbers and the following operations: addition, subtraction, division and multiplication.

Input
    A list of expressions to evaluate, one expression per line.

Output
    Print the result of the evaluation for each expression on separate lines. The result should be
    given with 2 digits of precision or "error" in the case that the expression was invalid.

Constraints
    Expressions can contain up to 100k operations

Example
    Given the following list of expressions:

Input
+ + 0.5 1.5 * 4 10
- 2e3 - 700 + 7 * 2 15
- -1.5 * 3.1415 / -7 -2
  100500
  1 2
+ 1
  
The expected output is:
Output
  42.00
  1337.00
  -12.50
  100500.00
  error
  error

`POST /calculate`

The expected response is a JSON array with the products in a 'data' wrapper.

Example: POST /calculate
{
    "expressions": [
        "+ + 0.5 1.5 * 4 10",
        "- 2e3 - 700 + 7 * 2 15",
        "- -1.5 * 3.1415 / -7 -2",
        "100500",
        "1 2",
        "+ 1"
    ]
}

Response:
{
    "results": [
        "42.00",
        "1337.00",
        "-12.50",
        "100500.00",
        "Error",
        "Error"
    ]
}

## Tools
Following are the tools needed to build and run the project:
- Maven
- Docker

## How to run
Following are the commands that are needed to run from project root directory

    $ mvn clean package
    $ docker build -t demo/demoapp .
    $ docker run -p 8081:8081 demo/demoapp

After that one can test with sample requests. Example is given in the following section. After finish run following commands:

    $ docker ps

Now you have the container id. Run the following command with container id:

    $ docker stop container_id

In fact, you can use `ctrl+c` which will stop the container.

## Sample Request
One can use postman to make the following request:

    POST http://localhost:8081/calculate
    Body
    {
        "expressions": [
            "+ + 0.5 1.5 * 4 10",
            "- 2e3 - 700 + 7 * 2 15",
            "- -1.5 * 3.1415 / -7 -2",
            "100500",
            "1 2",
            "+ 1"
        ]
    }

Or can use curl from command line (given that curl is available in your terminal):

    $ curl -d '{ "expressions": ["+ + 0.5 1.5 * 4 10", "- 2e3 - 700 + 7 * 2 15", "- -1.5 * 3.1415 / -7 -2", "100500", "1 2", "+ 1"]}' -H "Content-Type: application/json" -X POST  http://localhost:8081/calculate

## Test
For running test following is the command:

    $ mvn test