package my.test.notation;

import io.restassured.response.Response;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ApplicationTest {

	@Test
	public void testCalculate1() {
		String requestBody = "{\n"
				+ "    \"expressions\": [\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"100500\",\n"
				+ "        \"1 2\",\n"
				+ "        \"+ 1\"\n"
				+ "    ]\n"
				+ "}";

		Response response = given()
				.port(8081)
				.header("Content-type", "application/json")
				.and()
				.body(requestBody)
				.post("/calculate")
				.then()
				.extract().response();

		Assertions.assertEquals(200, response.statusCode());
		Assertions.assertEquals("[42.00, 1337.00, -12.50, 100500.00, Error, Error]",
				response.jsonPath().getString("results"));
	}

	@Test
	public void testCalculate2() {
		String requestBody = "{\n"
				+ "    \"expressions\": [\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"100500\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"100500\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"100500\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"+ + 0.5 1.5 * 4 10\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- 2e3 - 700 + 7 * 2 15\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"- -1.5 * 3.1415 / -7 -2\",\n"
				+ "        \"100500\",\n"
				+ "        \"1 2\",\n"
				+ "        \"+ 1\"\n"
				+ "    ]\n"
				+ "}";

		Response response = given()
				.port(8081)
				.header("Content-type", "application/json")
				.and()
				.body(requestBody)
				.post("/calculate")
				.then()
				.extract().response();

		Assertions.assertEquals(200, response.statusCode());
		Assertions.assertEquals("[42.00, 42.00, 42.00, 42.00, 42.00, 42.00, 1337.00, 1337.00, "
						+ "1337.00, 1337.00, 1337.00, 1337.00, -12.50, -12.50, -12.50, -12.50, -12.50, 100500.00, "
						+ "42.00, 42.00, 42.00, 42.00, 42.00, 42.00, 1337.00, 1337.00, 1337.00, 1337.00, 1337.00,"
						+ " 1337.00, -12.50, -12.50, -12.50, -12.50, -12.50, 100500.00, 42.00, 42.00, 42.00, "
						+ "42.00, 42.00, 42.00, 1337.00, 1337.00, 1337.00, 1337.00, 1337.00, 1337.00, -12.50, "
						+ "-12.50, -12.50, -12.50, -12.50, 100500.00, 42.00, 42.00, 42.00, 42.00, 42.00, 42.00, "
						+ "1337.00, 1337.00, 1337.00, 1337.00, 1337.00, 1337.00, -12.50, -12.50, -12.50, -12.50, "
						+ "-12.50, 100500.00, Error, Error]",
				response.jsonPath().getString("results"));
	}

	@Test
	public void testCalculate3() {
		String requestBody = "{\n"
				+ "    \"expressions\": [\n"
				+ "    ]\n"
				+ "}";

		Response response = given()
				.port(8081)
				.header("Content-type", "application/json")
				.and()
				.body(requestBody)
				.post("/calculate")
				.then()
				.extract().response();

		Assertions.assertEquals(200, response.statusCode());
		Assertions.assertEquals("[]",
				response.jsonPath().getString("results"));
	}

	@Test
	public void testCalculate4() {
		String requestBody = "{\n"
				+ "}";

		Response response = given()
				.port(8081)
				.header("Content-type", "application/json")
				.and()
				.body(requestBody)
				.post("/calculate")
				.then()
				.extract().response();

		Assertions.assertEquals(400, response.statusCode());
	}

	@Test
	public void testCalculate5() {
		Response response = given()
				.port(8081)
				.header("Content-type", "application/json")
				.and()
				.post("/calculate")
				.then()
				.extract().response();

		Assertions.assertEquals(400, response.statusCode());
	}

	@Test
	public void testCalculate6() {
		String requestBody = "{\n"
				+ "    \"expressions\": [\n"
				+ "        \"+ + 2e10 3e10 * 4 2e100\",\n"
				+ "        \"- 2e10 - 419 + 17 * 2 534\"\n"
				+ "    ]\n"
				+ "}";

		Response response = given()
				.port(8081)
				.header("Content-type", "application/json")
				.and()
				.body(requestBody)
				.post("/calculate")
				.then()
				.extract().response();

		Assertions.assertEquals(200, response.statusCode());
		Assertions.assertEquals("[8000000000000000000000000000000000000000000000000000"
						+ "0000000000000000000000000000000000000000000000000.00, 20000000666.00]",
				response.jsonPath().getString("results"));
	}
}
