package my.test.notation.service.utils;

import java.text.DecimalFormat;
import java.util.Stack;

/**
 *
 * Utility class to print the stack.
 *
 * Created by Oleksandr Romaniuk .
 */
public class StackUtils {
  /**
   * Formats the output in 10 decimal places and Prints the calculator stack.
   *
   * @param calculatorStack
   * @return
   */
  public static String printStack(Stack<Double> calculatorStack) {
    DecimalFormat fmt = new DecimalFormat("0.00");
    StringBuffer sb = new StringBuffer();

    calculatorStack.forEach((element) -> {
      sb.append(fmt.format(element));
    });

    return sb.toString();
  }
}
