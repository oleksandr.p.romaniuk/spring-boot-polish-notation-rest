package my.test.notation.service.exception;

public class CalculatorException extends Exception {
  public CalculatorException(String message) {
    super(message);
  }
}