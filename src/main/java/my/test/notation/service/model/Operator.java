package my.test.notation.service.model;

import my.test.notation.service.exception.CalculatorException;

import java.util.HashMap;

/**
 * Enumeration to represent the operators and their functions.
 * <p>
 * Created by Oleksandr Romaniuk.
 */
public enum Operator {
  /**
   * Addition operation.
   */
  ADD("+", 2) {
    @Override
    public Double operate(Double firstOperand, Double secondOperand) {
      return firstOperand + secondOperand;
    }
  },

  /**
   * Subtraction operation.
   */
  SUBTRACT("-", 2) {
    @Override
    public Double operate(Double firstOperand, Double secondOperand) {
      return  firstOperand - secondOperand;
    }
  },

  /**
   * Multiplication operation.
   */
  MUL("*", 2) {
    @Override
    public Double operate(Double firstOperand, Double secondOperand) {
      return  firstOperand * secondOperand;
    }
  },

  /**
   * Division operation.
   */
  DIV("/", 2) {
    @Override
    public Double operate(Double firstOperand, Double secondOperand) {
      return firstOperand / secondOperand;
    }
  };

  /**
   * Abstract method to be implemented by enum constants.
   *
   * @param firstOperand
   * @param secondOperand
   * @return
   * @throws CalculatorException
   */
  public abstract Double operate(Double firstOperand, Double secondOperand) throws CalculatorException;

  private String operator;
  private int numOfOperands;

  public String getOperator() {
    return operator;
  }

  public int getNumOfOperands() {
    return numOfOperands;
  }

  Operator(final String operator, final int numOfOperands) {
    this.operator = operator;
    this.numOfOperands = numOfOperands;
  }

  private static final HashMap<String, Operator> operatorMap = new HashMap<>();

  /**
   * Static block to initialize the map with key as operator symbol and value as Operator enum.
   */
  static {
    for (Operator o : values()) {
      operatorMap.put(o.getOperator(), o);
    }
  }

  /**
   * Returns an Operator instance based on symbol.
   *
   * @param symbol
   * @return
   */
  public static Operator getOperator(String symbol) {
    return operatorMap.get(symbol);
  }
}
