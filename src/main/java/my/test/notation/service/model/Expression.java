package my.test.notation.service.model;

import lombok.AllArgsConstructor;
import my.test.notation.service.exception.CalculatorException;

/**
 * Represents Expression of Polish Notation.
 * <p>
 * Created by Oleksandr Romaniuk.
 */

@AllArgsConstructor
public class Expression {
  private Double firstOperand;
  private Double secondOperand;
  private Operator operator;

  /**
   * Evaluates the operation based on operator.
   *
   * @return
   * @throws CalculatorException
   */
  public Double evaluate() throws CalculatorException {
    Double result = null;
    if (this.firstOperand != null) {
      result = operator.operate(firstOperand, secondOperand);
    }
    return result;
  }
}
