package my.test.notation.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents the last instruction that was executed.
 * <p>
 * Created by Oleksandr Romaniuk.
 */
@AllArgsConstructor
@Getter
public class LastInstruction {
  private Operator operator;
  private Double firstOperand;
  private Double secondOperand;
}
