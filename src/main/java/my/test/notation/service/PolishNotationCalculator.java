package my.test.notation.service;

import java.util.Arrays;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import my.test.notation.api.model.CalculationResult;
import my.test.notation.service.exception.CalculatorException;
import my.test.notation.service.model.Expression;
import my.test.notation.service.model.LastInstruction;
import my.test.notation.service.model.Operator;
import my.test.notation.service.utils.StackUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Stack;

/**
 * This class is responsible for handling all the operations of Calculator.
 * <p>
 * Created by Oleksandr Romaniuk.
 */
@Service
@Slf4j
public class PolishNotationCalculator implements Calculator {

  /**
   * Calculator Stack to maintain the LIFO-order of decimal numbers.
   */
  Stack<Double> calculatorStack = new Stack<>();

  /**
   * Stack to keep track of last instructions so that undo operation can refer.
   */
  Stack<LastInstruction> lastInstructions = new Stack();

  /**
   * Handles the computation for the input list.
   *
   * @param expressions
   */
  public CalculationResult compute(List<String> expressions) {
    CalculationResult result = new CalculationResult();
    clearAll();

    for (String expression : expressions) {
      String[] userInputArray = expression.trim().split("\\s+");
      Collections.reverse(Arrays.asList(userInputArray));
      List<String> inputList = Arrays.asList(userInputArray);
      try {
        String expressionResult = execute(inputList);
        clearAll();
        result.getResults().add(expressionResult);
      } catch (CalculatorException e) {
        result.getResults().add("Error");
      }
    }
    return result;
  }

  public String execute(List<String> inputList) throws CalculatorException {
    //for each item in the list apply the function.
    int pos = 0;
    for (String input : inputList) {
      evaluateToken(input, (++pos * 2 - 1));
    }
    return getResult();
  }

  /**
   * Evaluate the token : if a token is a digit push it to the stack otherwise if it is an operator,
   * pop only the required operands from stack and evaluate the expression. It also adds the last
   * instruction to another array to cater for undo operation.
   *
   * @param token
   * @throws CalculatorException
   */
  private void evaluateToken(String token, int position) throws CalculatorException {
    /*
    getDouble(token).ifPresentOrElse(aDouble -> {
          handleOperand(token);
        }, () -> {
          handleOperator(calculatorStack, token, position);
        });
  */

    if (isDouble(token)) {
      handleOperand(token);
    } else {
      handleOperator(calculatorStack, token, position);
    }
  }

  private boolean isDouble(String value) {
    try {
      Double.parseDouble(value);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Handles the digits by pushing it to Stack for further processing.
   *
   * @param token
   * @throws CalculatorException
   */
  private void handleOperand(String token) throws CalculatorException {
    Double decimalNumber;

    try {
      decimalNumber = Double.parseDouble(token);
    } catch (NumberFormatException nfe) {
      throw new CalculatorException(
          "An exception occurred while parsing the " + token + " to Double");
    }

    calculatorStack.push(decimalNumber);
    lastInstructions.push(new LastInstruction(null, decimalNumber, null));
  }

  /**
   * Responsible for handling the operators. If expression contains operator, it performs: i)  Pop
   * the last two elements from calculator stack ii) Performs the specified operation by using
   * {@link Expression} class.
   *
   * @param calculatorStack
   * @param token
   * @throws CalculatorException - If there are insufficient elements in the stack.
   */
  private void handleOperator(Stack<Double> calculatorStack, String token, int position)
      throws CalculatorException {
    Operator operator = getOperator(token);

    validateOperands(calculatorStack, position, operator);

    Double firstOperand = popElementFromStack(calculatorStack);
    Double secondOperand = (operator.getNumOfOperands() > 1) ? calculatorStack.pop() : null;

    Expression expression = new Expression(firstOperand, secondOperand, operator);
    Double result = expression.evaluate();

    lastInstructions.push(new LastInstruction(operator, firstOperand, secondOperand));

    if (result != null) {
      calculatorStack.push(result);
    }
  }

  /**
   * Validate if there are sufficient parameters to perform the operation.
   *
   * @param calculatorStack
   * @param position
   * @param operator
   * @throws CalculatorException
   */
  private void validateOperands(Stack<Double> calculatorStack, int position, Operator operator)
      throws CalculatorException {
    if (calculatorStack.size() < operator.getNumOfOperands()) {

      throw new CalculatorException("operator " + operator.getOperator() +
          " (position: " + position + "): insufficient parameters");
    }
  }

  /**
   * Returns an {{@link Operator}} if it matches the symbol otherwise throws an Exception.
   *
   * @param token
   * @return
   * @throws CalculatorException
   */
  private Operator getOperator(String token) throws CalculatorException {
    Operator operator = Operator.getOperator(token);
    if (operator == null) {
      throw new CalculatorException("No Operators found with symbol:" + token);
    }
    return operator;
  }

  /**
   * Clear all stacks.
   *
   * @return
   */
  private void clearAll() {
    this.calculatorStack.clear();
    this.lastInstructions.clear();
  }

  /**
   * Returns calculator stack.
   *
   * @return
   */
  public String getResult() throws CalculatorException {
    if (calculatorStack.size() > 1) {
      throw new CalculatorException("Error");
    }
    return StackUtils.printStack(calculatorStack);
  }

  private Double popElementFromStack(Stack<Double> stack) {
    Double element = null;
    if (!stack.empty()) {
      element = stack.pop();
    }
    return element;
  }
}
