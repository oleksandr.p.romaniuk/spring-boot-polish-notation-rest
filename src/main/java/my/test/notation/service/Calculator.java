package my.test.notation.service;

import java.util.List;
import my.test.notation.api.model.CalculationResult;
import my.test.notation.service.exception.CalculatorException;

/**
 * Generic Calculator class that defines a contract to implement any type of Calculator
 * <p>
 * Created by Oleksandr Romaniuk.
 */
public interface Calculator {

  /**
   * Performs postfix notation calculation which supports following operations:
   * <p>
   * <ul>
   * <li>ADD</li>
   * <li>SUBTRACT</li>
   * <li>MULTIPLY</li>
   * <li>DIVISION</li>
   * </ul>
   *
   * @param inputList
   * @throws CalculatorException
   */
  CalculationResult compute(List<String> inputList) throws CalculatorException;
}
