package my.test.notation.api.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class CalculationResult {
  List<String> results = new ArrayList<>();
}
