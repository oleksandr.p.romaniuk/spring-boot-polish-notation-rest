package my.test.notation.api.model;

import java.util.List;
import lombok.Data;

@Data
public class CalculationRequest {
  List<String> expressions;
}
