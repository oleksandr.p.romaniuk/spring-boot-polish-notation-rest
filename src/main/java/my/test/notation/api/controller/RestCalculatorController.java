package my.test.notation.api.controller;

import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import my.test.notation.api.model.CalculationRequest;
import my.test.notation.api.model.CalculationResult;
import my.test.notation.service.Calculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class RestCalculatorController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestCalculatorController.class);

	@Autowired
	private Calculator calculator;

	/**
	 * Expose "/calculate" endpoint which takes expressions.
	 *
	 * @param input
	 * @return
	 */
	@PostMapping("calculate")
	public ResponseEntity<CalculationResult> calculate(@RequestBody CalculationRequest calculationRequest) {
		List<String> inputList = calculationRequest.getExpressions();
		/**
		 * Call the compute method to calculate the results.
		 */
		LOGGER.info("POST /calculate { " + calculationRequest + " }");
		try {
			return new ResponseEntity<>(calculator.compute(inputList), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("POST /calculate {} - ERROR: {}", "error", e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}